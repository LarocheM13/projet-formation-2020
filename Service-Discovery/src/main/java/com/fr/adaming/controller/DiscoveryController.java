package com.fr.adaming.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.dto.RegisterDto;

@RestController
@RequestMapping(path = "/discovery")
public class DiscoveryController {
	
	private static List<RegisterDto> ALL_MICRO_SERVICES = new ArrayList<>();

	@PostMapping(path = "/register")
	public String register(@RequestBody RegisterDto dto) {
		ALL_MICRO_SERVICES.add(dto);
		System.err.println("DEBUG DEMANDE D'INSCRIPTION " + dto);
		return "MicroService registred!";
	}
	
	@GetMapping(path = "/{name}")
	public RegisterDto sendInfoByName(@PathVariable String name) {
		RegisterDto foundMicroService = null;
		for(RegisterDto ms : ALL_MICRO_SERVICES) {
			if(ms.getMicroServiceName().equals(name)) {
				foundMicroService = ms;
				break;
			}
		}
		return foundMicroService;
	}
	
	@GetMapping
	public List<RegisterDto> getAllAvailableMicroServices(){
		return ALL_MICRO_SERVICES;
	}
}