package com.fr.adaming.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class RegisterDto {
	
	private String addressIp;
	private String microServiceName;
	
	
}
