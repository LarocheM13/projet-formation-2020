package com.fr.adaming.converter;

import java.util.List;
import java.util.stream.Collectors;

import com.fr.adaming.dto.UserDto;
import com.fr.adaming.entity.User;

public class UserConverter {

	public static User convertToEntity(UserDto dto) {
		if (dto == null) {
			return null;
		}
		return new User(dto.getId(), dto.getLastName(), dto.getFirstName(), dto.getMail(), dto.getPassword(), dto.getRole(), dto.getSessionUrl());
	}
	
	public static UserDto convertToDto(User entity) {
		if(entity == null) {
			return null;
		}
		return new UserDto(entity.getId(), entity.getNom(), entity.getPrenom(), entity.getEmail(), entity.getPwd(), entity.getRole(), entity.getFormationUrl());
	}

	public static List<User> convertToEntity(List<UserDto> dtos) {
		if (dtos == null) {
			return null;
		}
		
		return dtos.stream().map(UserConverter::convertToEntity).collect(Collectors.toList());
	}
	
	public static List<UserDto> convertToDto(List<User> entities) {
		if(entities == null) {
			return null;
		}
		return entities.stream().map(UserConverter::convertToDto).collect(Collectors.toList());
	}
}
