package com.fr.adaming.converter;

import java.util.List;
import java.util.stream.Collectors;

import com.fr.adaming.dto.UserCreateDto;
import com.fr.adaming.entity.User;

public class UserCreateConverter {

	public static User convertToEntity(UserCreateDto dto) {
		if (dto == null) {
			return null;
		}
		return new User(dto.getLastName(), dto.getFirstName(), dto.getMail(), dto.getPassword(), dto.getRole());
	}
	
	public static UserCreateDto convertToDto(User entity) {
		if(entity == null) {
			return null;
		}
		return new UserCreateDto(entity.getNom(), entity.getPrenom(), entity.getEmail(), entity.getPwd(), entity.getRole());
	}

	public static List<User> convertToEntity(List<UserCreateDto> dtos) {
		if (dtos == null) {
			return null;
		}
		
		return dtos.stream().map(UserCreateConverter::convertToEntity).collect(Collectors.toList());
	}
	
	public static List<UserCreateDto> convertToDto(List<User> entities) {
		if(entities == null) {
			return null;
		}
		return entities.stream().map(UserCreateConverter::convertToDto).collect(Collectors.toList());
	}

}
