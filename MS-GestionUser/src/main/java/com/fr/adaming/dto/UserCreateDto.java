package com.fr.adaming.dto;

import com.fr.adaming.enumeration.RoleEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateDto {
	
	private String lastName;

	private String firstName;

	private String mail;

	private String password;

	private RoleEnum role;

}
