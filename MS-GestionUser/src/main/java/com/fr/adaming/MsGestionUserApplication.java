package com.fr.adaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import com.fr.adaming.dto.RegisterDto;

@SpringBootApplication
public class MsGestionUserApplication {

	private static final String DISCOVERY_BASE_URL = "http://localhost:9090/discovery";
	private static final String DISCOVERY_REGISTER_URL = DISCOVERY_BASE_URL + "/register";

	public static void main(String[] args) {
		SpringApplication.run(MsGestionUserApplication.class, args);

		RegisterDto thisMicroService = new RegisterDto("http://localhost:8080/user", "MS-GestionUser");
		
		RestTemplate rest = new RestTemplate();
		
		String discoveryResponse = rest.postForObject(DISCOVERY_REGISTER_URL, thisMicroService, String.class);
		
		System.err.println("Response Discovery ====> " + discoveryResponse);
	}

}

