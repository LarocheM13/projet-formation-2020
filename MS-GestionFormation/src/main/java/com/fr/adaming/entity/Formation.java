package com.fr.adaming.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Formation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String label;

	private LocalDate dateDebut;

	private LocalDate dateFin;

	private String nomFormateur;
	
	private String ville;
	
	private String agenceUrl;

	public Formation(String label, LocalDate dateDebut, LocalDate dateFin, String nomFormateur) {
		super();
		this.label = label;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nomFormateur = nomFormateur;
	}

}
