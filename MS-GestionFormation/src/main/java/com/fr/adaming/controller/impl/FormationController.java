package com.fr.adaming.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.IGenericController;
import com.fr.adaming.converter.FormationConverter;
import com.fr.adaming.converter.FormationCreateConverter;
import com.fr.adaming.dao.IFormationDao;
import com.fr.adaming.dto.FormationCreateDto;
import com.fr.adaming.dto.FormationDto;
import com.fr.adaming.dto.ResponseDto;
import com.fr.adaming.entity.Formation;

@RestController
public class FormationController implements IGenericController<FormationDto, FormationCreateDto>{

	@Autowired
	private IFormationDao dao;

	@Override
	public ResponseDto<FormationDto> create(FormationCreateDto dto) {
		// TODO catcher les erreurs
		return new ResponseDto<FormationDto>("CREATE : SUCCESS", FormationConverter.convertToDto(dao.save(FormationCreateConverter.convertToEntity(dto))), false);
	}

	@GetMapping(path = "/city/{city}")
	public ResponseDto<FormationDto> getAvailableFormationByCity(@PathVariable String city) {
		// TODO catcher les erreurs
		Formation f = dao.findFirstByOrderByDateDebutDesc();
		
		return new ResponseDto<FormationDto>("SUCCESS", FormationConverter.convertToDto(f), false);
		
	}

	@Override
	public ResponseDto<List<FormationDto>> readAll() {
		// TODO catcher les erreurs
		return new ResponseDto<List<FormationDto>>("READ ALL : SUCCESS", FormationConverter.convertToDto(dao.findAll()), false);
	}

	@Override
	public ResponseDto<FormationDto> readById(Long id) {
		// TODO catcher les erreurs
		return new ResponseDto<FormationDto>("READ BY ID : SUCCESS", FormationConverter.convertToDto(dao.findById(id).get()), false);
	}

	@Override
	public ResponseDto<FormationDto> update(FormationDto dto) {
		if(dto != null && dto.getId() != null && dao.existsById(dto.getId())) {
			return new ResponseDto<FormationDto>("UPDATE : SUCCESS", FormationConverter.convertToDto(dao.save(FormationConverter.convertToEntity(dto))), false);
		}
		return new ResponseDto<FormationDto>("UPDATE : FAIL", null, true);
	}

	@Override
	public ResponseDto<String> deleteById(Long id) {
		// TODO catcher les erreurs
		dao.deleteById(id);
		return new ResponseDto<String>("DELETE : SUCCESS", "Formation with id "+id+" has been deleted", false);
	}

}
