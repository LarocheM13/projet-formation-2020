package com.fr.adaming.converter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import com.fr.adaming.dto.FormationCreateDto;
import com.fr.adaming.entity.Formation;

public class FormationCreateConverter {

	public static Formation convertToEntity(FormationCreateDto dto) {
		if (dto == null) {
			return null;
		}
		return new Formation(dto.getLabel(), LocalDate.parse(dto.getStartDate()), LocalDate.parse(dto.getEndDate()), dto.getNomFormateur());
	}
	
	public static FormationCreateDto convertToDto(Formation entity) {
		if(entity == null) {
			return null;
		}
		return new FormationCreateDto(entity.getLabel(), entity.getDateDebut().toString(), entity.getDateFin().toString(), entity.getNomFormateur());
	}

	public static List<Formation> convertToEntity(List<FormationCreateDto> dtos) {
		if (dtos == null) {
			return null;
		}
		
		return dtos.stream().map(FormationCreateConverter::convertToEntity).collect(Collectors.toList());
	}
	
	public static List<FormationCreateDto> convertToDto(List<Formation> entities) {
		if(entities == null) {
			return null;
		}
		return entities.stream().map(FormationCreateConverter::convertToDto).collect(Collectors.toList());
	}

}
