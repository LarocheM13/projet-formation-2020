package com.fr.adaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import lombok.AllArgsConstructor;
import lombok.Data;

@SpringBootApplication
public class MsGestionFormationApplication {

	private static final String DISCOVERY_BASE_URL = "http://localhost:9090/discovery";
	private static final String DISCOVERY_REGISTER_URL = DISCOVERY_BASE_URL + "/register";

	public static void main(String[] args) {
		SpringApplication.run(MsGestionFormationApplication.class, args);

		RegisterDto thisMS = new RegisterDto("http://localhost:8082/formation", "MS-GestionFormation");
		
		RestTemplate rest = new RestTemplate();
		
		String discoveryResponse = rest.postForObject(DISCOVERY_REGISTER_URL, thisMS, String.class);
		
		System.err.println("Response Discovery ====> " + discoveryResponse);
	}

}

@Data
@AllArgsConstructor
class RegisterDto {
	private String addressIp;
	private String microServiceName;
}
