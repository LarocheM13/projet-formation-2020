package com.fr.adaming.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.IGenericController;
import com.fr.adaming.converter.AgenceConverter;
import com.fr.adaming.converter.AgenceCreateConverter;
import com.fr.adaming.dao.IAgenceDao;
import com.fr.adaming.dto.AgenceCreateDto;
import com.fr.adaming.dto.AgenceDto;
import com.fr.adaming.dto.ResponseDto;

@RestController
public class AgenceController implements IGenericController<AgenceDto, AgenceCreateDto> {

	@Autowired
	private IAgenceDao dao;

	@Override
	public ResponseDto<AgenceDto> create(AgenceCreateDto dto) {
		// TODO catcher les erreurs
		return new ResponseDto<AgenceDto>("CREATE : SUCCESS", AgenceConverter.convertToDto(dao.save(AgenceCreateConverter.convertToEntity(dto))), false);
	}

	@Override
	public ResponseDto<List<AgenceDto>> readAll() {
		// TODO catcher les erreurs
		return new ResponseDto<List<AgenceDto>>("READ ALL : SUCCESS", AgenceConverter.convertToDto(dao.findAll()), false);
	}

	@Override
	public ResponseDto<AgenceDto> readById(Long id) {
		// TODO catcher les erreurs
		return new ResponseDto<AgenceDto>("READ BY ID : SUCCESS", AgenceConverter.convertToDto(dao.findById(id).get()), false);
	}

	@Override
	public ResponseDto<AgenceDto> update(AgenceDto dto) {
		if(dto != null && dto.getId() != null && dao.existsById(dto.getId())) {
			return new ResponseDto<AgenceDto>("UPDATE : SUCCESS", AgenceConverter.convertToDto(dao.save(AgenceConverter.convertToEntity(dto))), false);
		}
		return new ResponseDto<AgenceDto>("UPDATE : FAIL", null, true);
	}

	@Override
	public ResponseDto<String> deleteById(Long id) {
		// TODO catcher les erreurs
		dao.deleteById(id);
		return new ResponseDto<String>("DELETE : SUCCESS", "Agence with id "+id+" has been deleted", false);
	}
}
